// El input le dice a angular que una propiedad de una clase va a poder venir de fuera
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html',
  styleUrls: ['./heroe-tarjeta.component.css']
})
export class HeroeTarjetaComponent implements OnInit {

  // Añadunos el decorador input, que hemos importado arriba desde @angular/core
  // Al hacer este input, podemos pasar esa propiedad de la clase desde el html
  // del componente padre (html.component.html) entre corchetes, con ese nombre
  @Input() heroe:any = {};
  @Input() index:number;

  // Aqui defino el nombre del evento que quiero que el padre en html este escuchando
  @Output() heroeSeleccionado: EventEmitter<number>;

  constructor(private router:Router) { 
    this.heroeSeleccionado = new EventEmitter();
  }

  ngOnInit() {
  }

  verHeroe(){
    // console.log(this.index);
    // this.router.navigate(['/heroe', this.index]);
    // Ahora en el metodo verHeroe, emito el indice del hijo al padre, gracias al output
    // que hemos importado
    this.heroeSeleccionado.emit(this.index);
  }
}
