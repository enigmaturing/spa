import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HeroesService } from "../../servicios/heroes.service";

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html'
})
export class HeroeComponent implements OnInit {

  heroe:any = {};

  constructor( private activatedRoute: ActivatedRoute,
               private heroesService: HeroesService) { 
    
    // Lectura 56: Recibir parámetro por url en el ts de el componente al que hemos navegado con un router
    this.activatedRoute.params.subscribe( params => { 
      console.log("el componente heroe.component.ts ha recibido el héroe con id: " + params['id']);
      // Ahora que el componente heroe.components.ts conoce la id del héroe al que se ha llamado,
      // podemos mosatrar datos sobre ese heroe:
      this.heroe = this.heroesService.getId(params['id']);
      console.log("el nombre del héroe con ese id es: " + this.heroe.nombre);
      console.log(this.heroe);
    } );

  }

  ngOnInit() {
  }

}
