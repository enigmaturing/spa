import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from "../../servicios/heroes.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {

  // variable local de este componente, en la que guardaremos los datos de nuestros heroes
  heroes:Heroe[] = [];

  // el constructor se ejecuta antes de ngOnInit() (antes de que la página esté cargada)
  constructor( private _heroesService:HeroesService,
               private router:Router) { 
  }

  // ngOnInit se ejecuta cuando la página ya esté cargada
  ngOnInit() {
    // almacenamos en nuestra variable local los datos de los heroes que se encuentran en el servicio, 
    // usando para ello el metodo getHeroes() de ese servicio
    this.heroes = this._heroesService.getHeroes();
    console.log(this.heroes);
  }

  // Este método es llamado por el (click) del boton de cada card del compoenente heroes.component.html
  verHeroe(idx:number){
    console.log("PULSADO: " + idx);
    this.router.navigate(['/heroe', idx]);
  }

}
