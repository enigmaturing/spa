import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HeroesService, Heroe } from "../../servicios/heroes.service";

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html'
})
export class ResultadosComponent implements OnInit {

  private heroes:Heroe[];
  private termino:string;

  constructor(private activatedRoute:ActivatedRoute,
              private heroesService:HeroesService) {
    // Lectura 56: Recibir parámetro por url en el ts de el componente al que hemos navegado con un router
    this.activatedRoute.params.subscribe( params => { 
      console.log("el componente heroe.resultados.ts ha recibido el siguiente termino a buscar: " + params['termino']);
      // Ahora que el componente heroe.resultados.ts conoce el termino a buscar, podemos usarlo
      this.heroes = this.heroesService.buscarHeroes(params['termino']);
      this.termino = params['termino'];
    } );
   }

  ngOnInit() {
  }

}
