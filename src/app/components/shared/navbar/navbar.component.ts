import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  // Este método es llamado por un click al boton buscar del navbar
  buscarHeroe(termino:string){
    console.log("buscando: " + termino);
    this.router.navigate(['/resultados', termino]);
  }
}
